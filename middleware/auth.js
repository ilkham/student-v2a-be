const userTokensRepo = require('../repositories/userTokensRepo');
const userSchoolsRepo = require('../repositories/userSchoolsRepo');

const jwt = require('jsonwebtoken');
const _ = require('lodash');

const auth = async (ctx, next) => {
  const { headers } = ctx.request;
  const payload = await validateToken({ headers });
  if (!payload) {
    const err = new Error('Invalid Token');
    ctx.status = 401;
    ctx.body = { error: err.message };
    return err;
  }
  dataSchool = await userSchoolsRepo.findSchoolByUserId(payload.user_id);
  if(!_.isEmpty(dataSchool)){
    ctx.schoolId = dataSchool.school_id
  }
  ctx.userId = payload.user_id;
  return next();
};
  
const validateToken = async ({ headers }) => {
    if (_.isEmpty(headers.authorization)) {
        return false;
    }

    const token = headers.authorization.replace('Bearer', '').trim();
    const data = await userTokensRepo.findToken(token);

    if (!_.isEmpty(data)) {
        const secretKey = process.env.SECRET_KEY;
        try {
            const payload = jwt.verify(token, secretKey);
            return payload;
          } catch (err) {
            return false
        }
    }
    return false;
};

module.exports = auth;
