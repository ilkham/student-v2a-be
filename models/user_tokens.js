module.exports = (sequelize, DataTypes) => {
    const userTokens = sequelize.define('userTokens', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            unique: true
        },
        app_name: {
            type: DataTypes.STRING,
        },
        token: {
            type: DataTypes.STRING,
        },
        user_id: {
                type: DataTypes.UUID,
        },
        device_identifier: {
            type: DataTypes.STRING,
        },
        device_type: {
            type: DataTypes.STRING,
        },
        user_agent: {
            type: DataTypes.STRING,
        }         
    },
    {
      tableName: 'user_tokens',
      timestamps: false
    });
    return userTokens;
  }
  
  