module.exports = (sequelize, DataTypes) => {
    const schools = sequelize.define('schools', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            unique: true
        },
        name: {
            type: DataTypes.STRING,
        },
        institution_id: {
            type: DataTypes.UUID,
        },
        level: {
            type: DataTypes.STRING,
        },
        phone: {
            type: DataTypes.STRING,
        },
        email: {
            type: DataTypes.STRING,
        }                 
    },
    {
      tableName: 'schools',
      timestamps: false
    });
    return schools;
  }
  
  