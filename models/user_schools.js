module.exports = (sequelize, DataTypes) => {
    const userSchools = sequelize.define('userSchools', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            unique: true
        },
        user_id: {
            type: DataTypes.UUID,
        },
        school_id: {
            type: DataTypes.UUID,
        },
        status: {
                type: DataTypes.STRING,
        }       
    },
    {
      tableName: 'user_schools',
      timestamps: false
    });
    return userSchools;
  }
  
  