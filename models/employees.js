module.exports = (sequelize, DataTypes) => {


    const employees = sequelize.define('employees', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            unique: true
        },
        full_name: {
            type: DataTypes.STRING,
        },
        first_name: {
                type: DataTypes.STRING,
        },
        last_name: {
            type: DataTypes.STRING,
        },
        gender: {
            type: DataTypes.STRING,
        }           
    },
    {
      tableName: 'employees',
      timestamps: false
    });
    return employees;
  }
  
  