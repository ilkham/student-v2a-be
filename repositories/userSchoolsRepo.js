const Db = require('../utils/db_context');

/* eslint-disable */
module.exports = {
    findSchoolByUserId: async (user_id) => {
        const db = await Db.getInstance();
        let result = await db.userSchools.findOne({
            where: {
                user_id
            }
        });
        return result;
    }
};
