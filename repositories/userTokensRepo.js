const Db = require('../utils/db_context');

/* eslint-disable */
module.exports = {
    findToken: async (token) => {
        const db = await Db.getInstance();
        let result = await db.userTokens.findOne({
            where: {
                token
            }
        });
        return result;
    }
};
