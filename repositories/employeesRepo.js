const Db = require('../utils/db_context');

/* eslint-disable */
module.exports = {
    find: async () => {
        const db = await Db.getInstance();
        console.log(db.employee)
        let result = await db.employees.findAll();
        return result;
    }
};
