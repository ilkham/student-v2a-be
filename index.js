"use strict";

const koa = require('koa')
require('dotenv').config()
const api = require('./route.js')
const DB = require('./utils/db_context');
const app = new koa()
// global.db = db
DB.initialize({
    connection_string: `${process.env.CONNECTION_STRING}`,
    models_path: '../models'
});
app.use(api.routes())
app.listen(process.env.PORT)
console.log(`[Koa2-Js] API is running ${process.env.PORT}`)


// const router = require('./route.js');
// router.route(app);
