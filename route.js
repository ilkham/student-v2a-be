"use strict";

const fs = require('fs');
const router = require('koa-router')()
const authMiddleware = require('./middleware/auth');
const controllers = {};
const files = fs.readdirSync(`${__dirname}/controllers/`);

files.forEach((fileName) => {
  const file = fileName.replace('.js', '');
  controllers[file] = require(`${__dirname}/controllers/${file}`);
});
router.get('/employees',authMiddleware, controllers.employeesController.index)
module.exports = router
