
const employeesRepo = require('../repositories/employeesRepo.js');
const Joi = require('joi');

module.exports = {
    index: async (ctx) => {
        // ctx.body = 'This is just an example'
        console.log("sami")
        try{
            const dataEmployee = await employeesRepo.find();
            if(!dataEmployee){
                throw new Error('order id not found');
            }
            ctx.body = dataEmployee;
        }catch(err)
        {
            const data = {
                status : 400,
                msg : err.message
            }
            console.log(data);
        }

    }
};
