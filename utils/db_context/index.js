"use strict";

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const options = {
    dialect: 'mysql',
    logging: process.env.NODE_ENV === 'production' ? false : console.log, // tslint:disable-line
    pool: {
        min: 0,
        max: 5,
        idle: 10000,
        acquire: 20000
    }
};
let configuration = null;
let modelsInitialized = false;
let models = {
    ORMProvider: Sequelize
};

exports.initialize = async ({ connection_string, models_path }) => {
    const sequelize = new Sequelize(connection_string, options);
    const modelsDir = path.join(__dirname, '../', models_path);
    fs.readdirSync(modelsDir)
        .filter((file) => {
            const fileExtension = file.slice(-3);
            const isEligible = (fileExtension === '.js');
            return (file.indexOf('.') !== 0) && isEligible;
        })
        .forEach((file) => {
            const model = sequelize.import(path.join(modelsDir, file));
            models[model.name] = model;
        });

    Object.keys(models).forEach((modelName) => {
        if (models[modelName].associate) {
            models[modelName].associate(models);
        }
    });

    models.ORMProvider = Sequelize;
    models.context = sequelize;
    modelsInitialized = true;
};

exports.getInstance = async () => {
    if (!modelsInitialized) {
        throw new Error('SQL Not initialize');
     }
    return models;
};

exports.startTransaction = async () => {
    if (!modelsInitialized) {
        throw new Error('SQL Not initialize');
     }
    models.db_transaction = await models.context.transaction({
        isolationLevel: models.ORMProvider.Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED
    });
};

exports.endTransaction = async () => {
    models.db_transaction = null;
};

exports.getTransaction = () => models.db_transaction;

exports.commit = async () => {
    if (models && models.db_transaction) {
        await models.db_transaction.commit();
        endTransaction();
    }
};

exports.rollback = async () => {
    if (models && models.db_transaction) {
        await models.db_transaction.rollback();
        endTransaction();
    }
};

exports.closeContext = async () => {
    let result = null;

    if (models && models.context) {
        console.info('Closing - DBContext'); // tslint:disable-line
        result = await models.context.close().catch((err) => {
            console.error(`Error Closing DBContext: ${err.stack}`); // tslint:disable-line
        });
        console.info('Closed - DBContext'); // tslint:disable-line
    }

    models = null;
    modelsInitialized = false;
    return result;
};

module.exports = exports;
